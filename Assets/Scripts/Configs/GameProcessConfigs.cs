﻿using UnityEngine;

[CreateAssetMenu(menuName = "Config/GameProcessConfigs", fileName = "GameProcessConfigs", order = 9)]
public class GameProcessConfigs : ScriptableObject
{
    [Header("Hero")] 
    public float CameraSmoothSpeedMove;
    public float SpeedMoveHero;

    [Header("Enemy")]
    public float RotationSpeedEnemy;
    public float MovementSpeedEnemy;
    public float CircleCreateEnemy = 3f;
    public float OffsetCreateEnemy = 3f;


    [Header("CreateEnemies")]
    public float DelayBeforeCreateEnemies = 2f;
    public float SpeedCreateEnemies = 1f;

    [Header("Global")]
    public float DelayShowGameOver = 1.2f;

}

