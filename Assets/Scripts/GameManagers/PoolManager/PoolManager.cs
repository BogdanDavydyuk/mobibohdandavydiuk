﻿using UnityEngine;

namespace Base
{
    public class PoolManager : BaseManager
    {
        private PoolMonoView _poolMonoView;

        public void SetPoolMonoView(PoolMonoView poolMonoView)
        {
            _poolMonoView = poolMonoView;
        }

        public T GetEffect<T>() where T : MonoBehaviour
        {
            return _poolMonoView.Factory.GetMonoBehaviour<T>();
        }

        public void ReleaseAllInstances()
        {
            _poolMonoView.Factory.ReleaseAllInstances();
        }

        public void Release<T>(T component) where T : MonoBehaviour
        {
            _poolMonoView.Factory.Release(component);
        }

    }
}