﻿using Base.Manager;

namespace Base
{
    public partial class GameManagersContexts 
    {
        public readonly PoolManager PoolManager;
        public readonly GameProcessManager GameProcessManager;
        public readonly ApplicationBehaviourManager ApplicationBehaviourManager;
        public readonly TimerManager TimerManager;
        public readonly ConfigsManager ConfigsManager;
    }
}
