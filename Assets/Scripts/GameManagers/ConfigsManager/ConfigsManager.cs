﻿using Base;
using UnityEngine;

public class ConfigsManager : BaseManager
{
    private GameProcessConfigs _gameProcessConfigs;

    public GameProcessConfigs GameProcessConfigs => _gameProcessConfigs;

    protected override void Initialize()
    {
        base.Initialize();

        _gameProcessConfigs = Resources.Load<GameProcessConfigs>("Configs/GameProcessConfigs");

        if (_gameProcessConfigs == null)
        {
            Debug.LogError("_gameProcessConfigs == null");
        }
    }
}

