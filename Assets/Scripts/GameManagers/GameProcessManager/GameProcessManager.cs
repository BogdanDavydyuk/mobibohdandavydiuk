using System;
using System.Collections;
using Base;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameProcessManager : BaseManager
{
    public Action OnHeroDie;
    public Action OnGameFinished;

    private HeroView _heroView;
    private IEnumerator _createEnemyCreator;

    private Transform _topPointOfField;
    private Transform _bottomPointOfField;
    private Transform _leftPointOfField;
    private Transform _rightPointOfField;

    public void SetHeroView(HeroView heroView)
    {
        _heroView = heroView;

        DOVirtual.DelayedCall(_managers.ConfigsManager.GameProcessConfigs.DelayBeforeCreateEnemies, () =>
        {
            _managers.TimerManager.StartTimer();
            _createEnemyCreator = CreateEnemies();
            GeneralContext.Behavior.StartCoroutine(_createEnemyCreator);
        });
    }

    public void SetFieldData(Transform topPoint, Transform bottomPoint, Transform leftPoint, Transform rightPoint)
    {
        _topPointOfField = topPoint;
        _bottomPointOfField = bottomPoint;
        _leftPointOfField = leftPoint;
        _rightPointOfField = rightPoint;
    }

    private IEnumerator CreateEnemies()
    {
        while (true)
        {
            var randomPos = Random.insideUnitSphere * _managers.ConfigsManager.GameProcessConfigs.CircleCreateEnemy;

            var x = randomPos.x + _heroView.transform.position.x + _managers.ConfigsManager.GameProcessConfigs.OffsetCreateEnemy;
            var y = _heroView.transform.position.y;
            var z = randomPos.z + _heroView.transform.position.z + _managers.ConfigsManager.GameProcessConfigs.OffsetCreateEnemy;

            float delayCreateNextEnemy = _managers.ConfigsManager.GameProcessConfigs.SpeedCreateEnemies;

            if (z > _topPointOfField.transform.position.z || z < _bottomPointOfField.transform.position.z || 
                x > _rightPointOfField.transform.position.x || x < _leftPointOfField.transform.position.x)
            {
                delayCreateNextEnemy = 0;
            }
            else
            {
                var enemyView = _managers.PoolManager.GetEffect<EnemyView>();
                enemyView.Init(new Vector3(x, y, z), _heroView.gameObject.transform, () => { _managers.PoolManager.Release(enemyView); });
                enemyView.gameObject.SetActive(true);
            }

            yield return new WaitForSecondsRealtime(delayCreateNextEnemy);
        }
    }


    public void HeroDie()
    {
        _managers.TimerManager.StopTimer();

        if (_createEnemyCreator != null)
        {
            GeneralContext.Behavior.StopCoroutine(_createEnemyCreator);
        }

        var deathEffect = _managers.PoolManager.GetEffect<DeathEffectMonoView>();
        deathEffect.Init(_heroView.transform.position);

        OnHeroDie?.Invoke();

        _heroView.gameObject.SetActive(false);

        DOVirtual.DelayedCall(_managers.ConfigsManager.GameProcessConfigs.DelayShowGameOver, () =>
        {
            _managers.PoolManager.ReleaseAllInstances();

            OnGameFinished?.Invoke();
        });

    }

    public void EnemyDie(Vector3 position)
    {
        var deathEffect = _managers.PoolManager.GetEffect<DeathEffectMonoView>();
        deathEffect.Init(position);
    }

}
