using System;

namespace Base
{
    public  abstract class BaseManager 
    {
        protected GameManagersContexts _managers;
        
        public static implicit operator bool(BaseManager value)
        {
            return value != null;
        }

        protected event Action INITIALIZE;

        private bool _initState;

        internal void InitializeObject(GameManagersContexts gameManagerContext)
        {
            _managers = gameManagerContext;
            _initState = true;
            InitializeData();
        }
        
        public virtual void PostInitializeObject()
        {
            Initialize();
            if (INITIALIZE != null) INITIALIZE.Invoke();
            _initState = false;
            Initialized = true;
        }

        protected virtual void InitializeData()
        {
            
        }
        
        protected virtual void Initialize()
        {
            if (!_initState)
            {
                throw new InvalidOperationException("can't invoke method directly");
            }
        }
        
        
        public virtual bool Initialized { get; private set; }

        public virtual void Dispose()
        {

        }
    }
}