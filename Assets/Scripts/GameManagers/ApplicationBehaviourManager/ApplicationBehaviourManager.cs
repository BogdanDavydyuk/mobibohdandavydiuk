﻿using System;

namespace Base.Manager
{
    public class ApplicationBehaviourManager : BaseManager
    {
        public event Action OnUpdate;

        public void Update()
        {
            OnUpdate?.Invoke();
        }
    }
}

