﻿using System.Collections.Generic;
using Base.Manager;

namespace Base
{
    public partial class GameManagersContexts : BaseContext
    {
        private readonly List<BaseManager> _managers = new List<BaseManager>();

        public GameManagersContexts()
        {
            InitializeService(PoolManager = new PoolManager());
            InitializeService(ApplicationBehaviourManager = new ApplicationBehaviourManager());
            InitializeService(GameProcessManager = new GameProcessManager());
            InitializeService(TimerManager = new TimerManager());
            InitializeService(ConfigsManager = new ConfigsManager());
        }

        public void Initialize()
        {
            foreach (var m in _managers)
            {
                m.InitializeObject(this);
            }

            PostInitialize();
        }

        private void PostInitialize()
        {
            foreach (var m in _managers)
            {
                m.PostInitializeObject();
            }
        }

        private void InitializeService(BaseManager service)
        {
            _managers.Add(service);
        }

        public override void Dispose()
        {
            foreach (var m in _managers)
            {
                m.Dispose();
            }

            _managers.Clear();
        }
    }
}
