﻿using System;
using Base;
using UnityEngine;

public class TimerManager : BaseManager
{
    private bool _isCheckTime;
    private float _currentTime;

    protected override void Initialize()
    {
        base.Initialize();
        _managers.ApplicationBehaviourManager.OnUpdate += OnUpdate;
    }

    public override void Dispose()
    {
        _managers.ApplicationBehaviourManager.OnUpdate -= OnUpdate;
    }

    private void OnUpdate()
    {
        if (_isCheckTime)
        {
            _currentTime += Time.deltaTime;
        }
    }


    public void StartTimer()
    {
        ClearTime();
        _isCheckTime = true;
    }

    public void StopTimer()
    {
        _isCheckTime = false;
    }

    public string GetLastCheckedTime()
    {
        var time = TimeSpan.FromSeconds(_currentTime);
        
        return time.ToString(@"mm\:ss\:fff");
    }

    private void ClearTime()
    {
        _currentTime = 0;
    }
}

