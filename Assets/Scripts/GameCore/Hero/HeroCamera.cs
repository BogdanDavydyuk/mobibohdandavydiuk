using Base;
using UnityEngine;

public class HeroCamera : BaseMonoView
{
    [SerializeField] private Transform _heroTransform;

    private Vector3 _offset;

    private void Start()
    {
        _offset = transform.position - _heroTransform.position;
    }

    private void FixedUpdate()
    {
        Vector3 desiredPosition = _heroTransform.position + _offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, _managers.ConfigsManager.GameProcessConfigs.CameraSmoothSpeedMove);
        transform.position = smoothedPosition;
    }

    protected override void SetMonoContent()
    {
        
    }
}