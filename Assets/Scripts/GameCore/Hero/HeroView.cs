using Base;
using UnityEngine;
public class HeroView : BaseMonoView
{
    [SerializeField] private Animator _animator;
    [SerializeField] private FloatingJoystick _floatingJoystick;

    private eTypeAnimTriggersHero _animatorTriggerCurrent;

    protected override void SetMonoContent()
    {
        _managers.GameProcessManager.SetHeroView(this);
    }

    private void Start()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 300;

        _floatingJoystick.OnJoystickStateChanged += JoystickStateChanged;
    }

    private void OnDestroy()
    {
        _floatingJoystick.OnJoystickStateChanged -= JoystickStateChanged;
    }

    private void OnCollisionEnter(Collision collision)//collision with any enemy start
    {
        if (collision.gameObject.tag == "Enemy")
        {
            _managers.GameProcessManager.HeroDie();
        }

        if (collision.gameObject.tag == "Bomb")
        {
            _managers.GameProcessManager.HeroDie();
        }
    }

    private void JoystickStateChanged(bool state)
    {
        var newState = state ? eTypeAnimTriggersHero.Run : eTypeAnimTriggersHero.Idle;

        if (newState == _animatorTriggerCurrent) return;

        _animator?.ResetTrigger(_animatorTriggerCurrent.ToString());

        _animatorTriggerCurrent = newState;

        _animator?.SetTrigger(_animatorTriggerCurrent.ToString());
    }
}

public enum eTypeAnimTriggersHero
{
    Idle,
    Run,
}