using System;
using Base;
using UnityEngine;

public class EnemyView : BaseMonoView
{
    [SerializeField] private Animator _animator;

    public Transform player;

    private float _rotationSpeed, _movementSpeed;
    private Action _onComplete;
    private bool _isFollow;

    protected override void SetMonoContent() { }

    private void OnCollisionEnter(Collision collision)//collision with any enemy start
    {
        if (collision.gameObject.tag == "Bomb")
        {
            _managers.GameProcessManager.EnemyDie(transform.position);
            Release();
        }
    }

    private void FixedUpdate()
    {
        if (!_isFollow) return;

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(transform.position - player.position), _rotationSpeed * Time.deltaTime);

        transform.position += -transform.forward * _movementSpeed * Time.deltaTime;
    }

    public void Init(Vector3 startPos, Transform heroTransform, Action onComplete)
    {
        _rotationSpeed = _managers.ConfigsManager.GameProcessConfigs.RotationSpeedEnemy;
        _movementSpeed = _managers.ConfigsManager.GameProcessConfigs.MovementSpeedEnemy;

        transform.position = startPos;
        player = heroTransform;
        _onComplete = onComplete;

        _animator?.SetTrigger(eTypeAnimTriggersHero.Run.ToString());

        _isFollow = true;

        _managers.GameProcessManager.OnHeroDie += HeroDied;
    }

    public void Release()
    {
        _managers.GameProcessManager.OnHeroDie -= HeroDied;
        _isFollow = false;
        _onComplete?.Invoke();
    }

    private void HeroDied()
    {
        if (_animator != null)
        {
            _animator.SetTrigger(eTypeAnimTriggersHero.Idle.ToString());
        }

        _isFollow = false;
    }

}
