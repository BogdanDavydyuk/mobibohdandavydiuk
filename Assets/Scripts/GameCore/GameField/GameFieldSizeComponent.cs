﻿using UnityEngine;


  [ExecuteInEditMode]
  public class GameFieldSizeComponent : MonoBehaviour
  {
    public int Height = 10;
    public Transform Transform;
    public int Width = 11;

    private void Update()
    {
      if (Transform != null)
      {
        Transform.localScale = new Vector3(Width, Height * Mathf.Sqrt(2f), 0);
        Transform.localPosition = new Vector3(Width / 2f, 0, Height * Mathf.Sqrt(2f) / 2f);

        var renderer = Transform.GetComponent<MeshRenderer>();
        if (renderer != null) renderer.sharedMaterial.mainTextureScale = new Vector2(Width / 2f, Height / 2f);
      }
    }
  }
