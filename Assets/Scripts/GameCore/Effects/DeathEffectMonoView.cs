﻿using Base;
using UnityEngine;

public class DeathEffectMonoView : BaseMonoView
{
    protected override void SetMonoContent() { }

    public void Init(Vector3 startPos)
    {
        transform.position = startPos;
    }
}

