﻿using System;
using UnityEngine;

namespace Base
{
    public static class GeneralContext
    {
        public static bool IsInit => _isCreate;
        public static ApplicationBehaviour Behavior => _behavior;
        public static GameManagersContexts GameManagers => _gameManagers;

        private static ApplicationBehaviour _behavior;
        private static bool _isCreate;
        public static event Action OnContextInit;
        private static GameManagersContexts _gameManagers;

        public static void CreateContext(GameObject go)
        {
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 300;

            if (_isCreate && _gameManagers != null) return;
            _isCreate = true;

            _behavior = go.GetComponent<ApplicationBehaviour>();
            if (_behavior == null)
            {
                _behavior = go.AddComponent<ApplicationBehaviour>();
            }

            _gameManagers = new GameManagersContexts();

            _gameManagers.Initialize();

            OnContextInit?.Invoke();
        }

        public static void Update()
        {
            if (!IsInit) return;

            _gameManagers.ApplicationBehaviourManager.Update();
        }
    }
}