using System;
using UnityEngine;

namespace Base
{
    public abstract class BaseMonoView: MonoBehaviour
    {
        protected GameManagersContexts _managers;

        protected bool IsInit;

        public static event Action OnInit;
        
        protected virtual void Awake()
        {
            if (GeneralContext.IsInit)
            {
                _managers = GeneralContext.GameManagers;
                OnInitContext();
            }
            else
            {
                GeneralContext.OnContextInit += OnInitContext;
            }
        }

        private void OnInitContext()
        {
            _managers = GeneralContext.GameManagers;
            GeneralContext.OnContextInit -= OnInitContext;
            IsInit = true;
            SetMonoContent();

            if (OnInit != null)
            {
                OnInit.Invoke();
            }
        }

        protected abstract void SetMonoContent();

    }
}