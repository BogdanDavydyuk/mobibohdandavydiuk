﻿using Base.UI;
using UnityEngine;

namespace Base
{
    public class PoolMonoView : BaseMonoView
    {
        [SerializeField] private MultiplyComponentPoolFactory _multiply;

        public MultiplyComponentPoolFactory Factory => _multiply;

        protected override void SetMonoContent()
        {
            _managers.PoolManager.SetPoolMonoView(this);

        }
    }
}