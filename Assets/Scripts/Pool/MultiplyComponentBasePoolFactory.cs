using System;
using System.Collections.Generic;
using UnityEngine;

namespace Base.UI.Pool
{
    public class MultiplyComponentBasePoolFactory : BasePoolFactory
    {
        [SerializeField]
        private GameObject[] _prefabs;
        [SerializeField]
        private int _count;
        [SerializeField]
        private Transform _content;
        [SerializeField]
        private Transform _poolStorage;

        private readonly Dictionary<Type, HashSet<GameObject>> _instances;
        private Dictionary<Type, Queue<GameObject>> _pool;

        public Transform Content { get { return _content; } }

        public MultiplyComponentBasePoolFactory()
        {
            _instances = new Dictionary<Type, HashSet<GameObject>>();
            _pool = new Dictionary<Type, Queue<GameObject>>();
        }

        public int CountInstances
        {
            get { return _instances.Count; }
        }

        private void Awake()
        {
            if (_instances.Count > 0)
                return;

            for (int i = 0; i < _count; i++)
            {
                Get<Transform>();
            }
            ReleaseAllInstances();
        }
       

        public override T Get<T>()
        {
            return Get<T>(_instances.Count);
        }
        
        public T GetMonoBehaviour<T>() where T : MonoBehaviour
        {
            return Get<T>(_instances.Count);
        }
        
        public T Get<T>(Transform parent) where T : Component
        {
            return Get<T>(_instances.Count , parent);
        }

        public T Get<T>(int siblingIndex, Transform parent = default) where T : Component
        {
            bool isNewInstance = false;

            if (GetPool(typeof(T)).Count == 0)
            {
                GameObject result = GetInstance<T>();
                GetPool(typeof(T)).Enqueue(result);
                isNewInstance = true;
            }

            T resultComponent = GetPool(typeof(T)).Dequeue().GetComponent<T>();
            if (null == resultComponent)
            {
                return resultComponent;
            }

            var go = resultComponent.gameObject;
            var t = resultComponent.transform;
            if (isNewInstance || (_poolStorage != null && _poolStorage != _content))
            {
                if (parent != default)
                {
                    t.SetParent(parent, false);
                }
                else
                {
                    t.SetParent(_content, false);
                }
            }

            GetInstances(typeof(T)).Add(go);

            if (!go.activeSelf)
            {
                go.SetActive(true);
            }

            if (t.GetSiblingIndex() != siblingIndex)
            {
                t.SetSiblingIndex(siblingIndex);
            }

            return resultComponent;
        }

        public GameObject GetInstance<T>() where T : Component
        {
            foreach (var prefab in _prefabs)
            {
                var component = prefab.gameObject.GetComponent(typeof(T));
                if (component != null)
                {
                    return Instantiate(prefab);
                }
            }

            return null;
        }

        public override void Release<T>(T component)
        {
            var go = component.gameObject;
            if (GetInstances(typeof(T)).Contains(go))
            {
                go.SetActive(false);
                if (_poolStorage)
                {
                    go.transform.SetParent(_poolStorage, false);
                }

                GetPool(typeof(T)).Enqueue(go);
                GetInstances(typeof(T)).Remove(go);
            }
        }
        
        public void ReleaseComponent(Component component)
        {
            Type type = component.GetType();

            var go = component.gameObject;
            if (GetInstances(type).Contains(go))
            {
                go.SetActive(false);
                if (_poolStorage)
                {
                    go.transform.SetParent(_poolStorage, false);
                }

                GetPool(type).Enqueue(go);
                GetInstances(type).Remove(go);
            }
        }

        public void ReleaseAllInstances()
        {
            foreach (var hashSet in _instances)
            {
                foreach (GameObject instance in hashSet.Value)
                {
                    instance.SetActive(false);
                    GetPool(hashSet.Key).Enqueue(instance);
                }
            }

            _instances.Clear();
        }

        public void Dispose()
        {
            ReleaseAllInstances();

            foreach (var queue in _pool.Values)
            {
                foreach (GameObject gameObject in queue)
                {
                    GameObject.Destroy(gameObject);
                }
            }

            _pool.Clear();
        }

        private Queue<GameObject> GetPool(Type type)
        {
            Queue<GameObject> result;

            if (!_pool.TryGetValue(type, out result))
            {
                var newQueue = result = new Queue<GameObject>();
                _pool.Add(type, newQueue);
            }

            return result;
        }

        private HashSet<GameObject> GetInstances(Type type)
        {
            HashSet<GameObject> result;

            if (!_instances.TryGetValue(type, out result))
            {
                var newHashSet = result = new HashSet<GameObject>();
                _instances.Add(type, newHashSet);
            }

            return result;
        }
    }
}