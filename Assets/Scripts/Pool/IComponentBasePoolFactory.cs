﻿using UnityEngine;

namespace Base.UI.Pool
{
    public abstract class BasePoolFactory : MonoBehaviour
    {
        public abstract T Get<T>() where T : Component;

        public abstract void Release<T>(T component) where T : Component;
    }
}