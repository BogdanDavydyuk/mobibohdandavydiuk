using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LobbyUIView : MonoBehaviour
{
    [SerializeField] private Button _playButton;

    private void Start()
    {
        _playButton.onClick.AddListener(PlayClick);
    }

    private void OnDestroy()
    {
        _playButton.onClick.RemoveListener(PlayClick);
    }

    private void OnEnable()
    {
        _playButton.transform.GetComponent<Image>().color = Color.green;
    }

    private void PlayClick()
    {
        SceneManager.LoadScene("Game");
    }
}
