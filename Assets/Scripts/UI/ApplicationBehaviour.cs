using UnityEngine;

namespace Base
{
    public class ApplicationBehaviour : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);

            GeneralContext.CreateContext(gameObject);
        }

        private void Update()
        {
            GeneralContext.Update();
        }

        private void OnApplicationPause(bool pause)
        {
    
        }
    }
}