using Base;
using UnityEngine;

public class GameUIView : BaseMonoView
{
    [SerializeField] private GameOverWindowView _gameOverWindowView;
    [SerializeField] private Transform _topPointOfField;
    [SerializeField] private Transform _bottomPointOfField;
    [SerializeField] private Transform _leftPointOfField;
    [SerializeField] private Transform _rightPointOfField;

    protected override void SetMonoContent()
    {
        _managers.GameProcessManager.OnGameFinished += OnGameFinished;
        _managers.GameProcessManager.SetFieldData(_topPointOfField, _bottomPointOfField, _leftPointOfField, _rightPointOfField);
    }

    private void OnDestroy()
    {
        _managers.GameProcessManager.OnGameFinished -= OnGameFinished;
    }

    private void OnEnable()
    {
        _gameOverWindowView.gameObject.SetActive(false);
    }

    private void OnGameFinished()
    {
        _gameOverWindowView.gameObject.SetActive(true);
    }

   
}
