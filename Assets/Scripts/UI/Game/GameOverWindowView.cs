using Base;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverWindowView : BaseMonoView
{
    [SerializeField] private Button _restartButton;
    [SerializeField] private TextMeshProUGUI _resultText;

    protected override void SetMonoContent() { }

    private void Start()
    {
        _restartButton.onClick.AddListener(RestartClick);
    }

    private void OnEnable()
    {
        _resultText.text = _managers.TimerManager.GetLastCheckedTime();
    }

    private void OnDestroy()
    {
        _restartButton.onClick.RemoveListener(RestartClick);
    }

    private void RestartClick()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
